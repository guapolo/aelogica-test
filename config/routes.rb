Rails.application.routes.draw do
  resources :units, only: [:index] do
    collection do
      post :etl
    end
  end

  root to: 'units#index'
end
