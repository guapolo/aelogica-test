class UnitGroupsJsonProcessorWorker
  include Sidekiq::Worker

  def perform(file_path)
    Etl::ProcessUnitGroupsJson.call(file_path, Unit)
  end
end
