class UnitsCsvProcessorWorker
  include Sidekiq::Worker

  def perform(file_path)
    Etl::ProcessUnitsCsv.call(file_path, Unit)
  end
end
