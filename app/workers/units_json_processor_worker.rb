class UnitsJsonProcessorWorker
  include Sidekiq::Worker

  def perform(file_path)
    Etl::ProcessUnitsJson.call(file_path, Unit)
  end
end
