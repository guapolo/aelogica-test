class Unit < ApplicationRecord
  validates :area, :description, :name, :price, :uom,
    presence: true
end
