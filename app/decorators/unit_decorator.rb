class UnitDecorator < Draper::Decorator

  delegate :persisted?, :id, :name, :description, :uom

  def area
    "#{model.area} #{model.uom}"
  end

  def price
    helpers.number_to_currency(model.price)
  end
end
