class UnitsController < ApplicationController
  def index
    @units = Unit.order(name: :asc).decorate
  end

  def etl
    Dir['data/csv/*.csv'].each{ |csv_file| UnitsCsvProcessorWorker.perform_async(csv_file) }
    Dir['data/json/*_units.json'].each{ |json_file| UnitsJsonProcessorWorker.perform_async(json_file) }
    Dir['data/json/*unit_groups.json'].each{ |json_file| UnitGroupsJsonProcessorWorker.perform_async(json_file) }
  end
end
