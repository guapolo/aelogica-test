require 'light-service'
require 'etl/version'
require 'etl/extract_from_json'
require 'etl/extract_units_from_csv'
require 'etl/extract_unit_groups_from_json'
require 'etl/extract_units_from_json'
require 'etl/load_units_data'
require 'etl/process_units_csv'
require 'etl/process_unit_groups_json'
require 'etl/process_units_json'

module Etl
  DEFAULT_UOM = "sqm".freeze
end
