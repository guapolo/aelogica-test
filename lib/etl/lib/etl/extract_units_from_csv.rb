require 'csv'

module Etl
  class ExtractUnitsFromCsv
    extend ::LightService::Action

    expects  :csv_file_path
    promises :units_array


    executed do |ctx|
      if File.readable?(ctx.csv_file_path)
        ctx[:units_array] = []
        CSV.foreach(ctx.csv_file_path, headers: true).with_index(1) do |row, line|
          ctx[:units_array] << {
            area:        row['area']&.to_f,
            description: row['description'],
            price:       row['price']&.to_f,
            name:        row['name'],
            uom:         Etl::DEFAULT_UOM
          }
        end
      else
        ctx.fail!("CSV file is not readable (#{ctx.csv_file_path})")
      end
    end
  end
end