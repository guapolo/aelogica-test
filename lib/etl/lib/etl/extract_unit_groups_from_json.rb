module Etl
  class ExtractUnitGroupsFromJson
    include ExtractFromJson

    def self.json_units_array_key
      'unit_groups'
    end
  end
end