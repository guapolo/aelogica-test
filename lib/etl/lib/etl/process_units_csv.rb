module Etl
  class ProcessUnitsCsv
    extend ::LightService::Organizer

    class << self
      def call(csv_file_path, ar_loader_class)
        with(csv_file_path: csv_file_path, ar_loader_class: ar_loader_class).reduce(actions)
      end

      def actions
        [
          ExtractUnitsFromCsv,
          LoadUnitsData
        ]
      end
    end
  end
end