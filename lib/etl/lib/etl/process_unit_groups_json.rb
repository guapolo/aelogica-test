module Etl
  class ProcessUnitGroupsJson
    extend ::LightService::Organizer

    class << self
      def call(json_file_path, ar_loader_class)
        with(json_file_path: json_file_path, ar_loader_class: ar_loader_class).reduce(actions)
      end

      def actions
        [
          ExtractUnitGroupsFromJson,
          LoadUnitsData
        ]
      end
    end
  end
end