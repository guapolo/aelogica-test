module Etl
  class LoadUnitsData
    extend ::LightService::Action

    expects  :units_array, :ar_loader_class

    # ar_loader_class should respond to save, attr, etc.

    executed do |ctx|
      ctx.ar_loader_class.transaction do
        ctx.units_array.each do |unit_attributes|
          obj = ctx.ar_loader_class.create(unit_attributes)
          unless obj.persisted?
            ctx.fail!("Object not saved: #{obj.errors.full_messages.join(', ')}. Attributes: #{unit_attributes}")
          end
        end
      end
    end
  end
end