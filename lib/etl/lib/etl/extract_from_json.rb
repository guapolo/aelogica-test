require 'json'

module Etl
  module ExtractFromJson
    def self.included(base)
      base.send :extend, ::LightService::Action
      base.class_eval do
        expects  :json_file_path
        promises :units_array

        executed do |ctx|
          if File.readable?(ctx.json_file_path)
            ctx[:units_array] = []
            json = JSON::parse(File.read(ctx.json_file_path))
            json[self.json_units_array_key]&.each do |unit_values|
              ctx[:units_array] << {
                area:        unit_values['area']&.to_f,
                description: unit_values['description'],
                price:       unit_values['price']&.to_f,
                name:        unit_values['name'],
                uom:         Etl::DEFAULT_UOM
              }
            end
          else
            ctx.fail!("JSON file is not readable (#{ctx.json_file_path})")
          end
        end
      end
    end
  end
end
