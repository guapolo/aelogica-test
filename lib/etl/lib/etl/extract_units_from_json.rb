module Etl
  class ExtractUnitsFromJson
    include ExtractFromJson

    def self.json_units_array_key
      'units'
    end
  end
end