RSpec.describe Etl do
  it "has a version number" do
    expect(Etl::VERSION).not_to be nil
  end

  it "has a default unit of measure" do
    expect(Etl::DEFAULT_UOM).not_to be nil
  end
end
