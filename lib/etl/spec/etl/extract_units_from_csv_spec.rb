RSpec.describe Etl::ExtractUnitsFromCsv do
  let!(:ar_loader_class){ double("ActiveRecordClass") }
  let(:context) do
    LightService::Testing::ContextFactory
      .make_from(Etl::ProcessUnitsCsv)
      .for(described_class)
      .with(csv_file, ar_loader_class)
  end

  context 'when the given file isn\'t readable' do
    let!(:csv_file){'spec/support/data/csv/no-dice.csv'}
    let!(:result){ described_class.execute(context) }

    it 'fails miserably' do
      expect(result).to be_failure
    end

    it 'fails with an error message' do
      expect(result.message).to eq "CSV file is not readable (#{csv_file})"
    end
  end

  context 'when the given file is readable' do
    let!(:csv_file){'spec/support/data/csv/units.csv'}
    let!(:result){ described_class.execute(context) }

    it 'succeeds' do
      expect(result).to be_success
    end


    describe 'units_array' do
      context 'when the read file contains data' do
        it 'contains hashes with the right set of keys' do
          unit_hash = result.units_array.first
          expect(unit_hash.keys).to eq [:area, :description, :price, :name, :uom]
        end
      end

      context 'when the read file is empty' do
        let!(:csv_file){'spec/support/data/csv/units_empty.csv'}

        it 'it is empty' do
          expect(result.units_array).to be_empty
        end
      end
    end
  end
end