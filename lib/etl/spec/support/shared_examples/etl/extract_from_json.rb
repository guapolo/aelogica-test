RSpec.shared_examples "Extract from JSON" do |process_class, json_file|
  let!(:ar_loader_class){ double("ActiveRecordClass") }
  let!(:json_data_dir){ File.join(__dir__, '..', '..', 'data', 'json') }
  let!(:json_file_path){ File.join(json_data_dir, json_file)}
  let(:context) do
    LightService::Testing::ContextFactory
      .make_from(process_class)
      .for(described_class)
      .with(json_file_path, ar_loader_class)
  end

  context 'when the given file isn\'t readable' do
    let!(:json_file_path){ File.join(json_data_dir, 'no-dice.json')}
    let!(:result){ described_class.execute(context) }

    it 'fails miserably' do
      expect(result).to be_failure
    end

    it 'fails with an error message' do
      expect(result.message).to eq "JSON file is not readable (#{json_file_path})"
    end
  end

  context 'when the given file is readable' do
    let!(:result){ described_class.execute(context) }

    it 'succeeds' do
      expect(result).to be_success
    end


    describe 'units_array' do
      context 'when the read file contains data' do
        it 'contains hashes with the right set of keys' do
          unit_hash = result.units_array.first
          expect(unit_hash.keys).to eq [:area, :description, :price, :name, :uom]
        end
      end

      context 'when the read file is empty' do
        let!(:json_file_path){ File.join(json_data_dir, "#{File.basename(json_file, '.json')}_empty.json")}

        it 'it is empty' do
          expect(result.units_array).to be_empty
        end
      end
    end
  end
end