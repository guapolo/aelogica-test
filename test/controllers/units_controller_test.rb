require 'test_helper'

class UnitsControllerTest < ActionDispatch::IntegrationTest
  def units_csv_files
    1
  end

  def unit_groups_files
    1
  end

  def units_json_files
    10
  end

  test "should get index" do
    get units_url
    assert_response :success
  end

  test "should post to the etl action" do
    post etl_units_url
    assert_equal units_csv_files, UnitsCsvProcessorWorker.jobs.size
    assert_equal unit_groups_files, UnitGroupsJsonProcessorWorker.jobs.size
    assert_equal units_json_files, UnitsJsonProcessorWorker.jobs.size
    assert_response :success
  end

end
