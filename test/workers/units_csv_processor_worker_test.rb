require 'test_helper'
class UnitsCsvProcessorWorkerTest < ActiveSupport::TestCase
  test "job is performed correctly" do
    Unit.delete_all
    UnitsCsvProcessorWorker.clear
    assert_equal 0, Unit.count
    assert_equal 0, UnitsCsvProcessorWorker.jobs.size
    UnitsCsvProcessorWorker.perform_async(File.join(__dir__, '..', 'fixtures', 'files', 'data', 'csv', 'units.csv'))
    assert_equal 1, UnitsCsvProcessorWorker.jobs.size
    UnitsCsvProcessorWorker.drain
    assert_equal 0, UnitsCsvProcessorWorker.jobs.size
    assert_equal 1, Unit.count
  end
end
