require 'test_helper'
class UnitGroupsJsonProcessorWorkerTest < ActiveSupport::TestCase
  test "job is performed correctly" do
    Unit.delete_all
    UnitGroupsJsonProcessorWorker.clear
    assert_equal 0, Unit.count
    assert_equal 0, UnitGroupsJsonProcessorWorker.jobs.size
    UnitGroupsJsonProcessorWorker.perform_async(File.join(__dir__, '..', 'fixtures', 'files', 'data', 'json', 'unit_groups.json'))
    assert_equal 1, UnitGroupsJsonProcessorWorker.jobs.size
    UnitGroupsJsonProcessorWorker.drain
    assert_equal 0, UnitGroupsJsonProcessorWorker.jobs.size
    assert_equal 1, Unit.count
  end
end
