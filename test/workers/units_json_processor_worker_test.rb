require 'test_helper'
class UnitsJsonProcessorWorkerTest  < ActiveSupport::TestCase
  test "job is performed correctly" do
    Unit.delete_all
    UnitsJsonProcessorWorker.clear
    assert_equal 0, Unit.count
    assert_equal 0, UnitsJsonProcessorWorker.jobs.size
    UnitsJsonProcessorWorker.perform_async(File.join(__dir__, '..', 'fixtures', 'files', 'data', 'json', 'units.json'))
    assert_equal 1, UnitsJsonProcessorWorker.jobs.size
    UnitsJsonProcessorWorker.drain
    assert_equal 0, UnitsJsonProcessorWorker.jobs.size
    assert_equal 1, Unit.count
  end
end
