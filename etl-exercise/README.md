# ETL Exercise

### Objective

Simulate a basic extract, transform, and load (ETL) process using standard tools.
ETL involves connecting to a data source (e.g. a web API or a structured
file like CSV), parsing its data, formatting the data if needed, and loading the
processed data into a database.

For this exercise, we expect you to create the following:

1. a Ruby gem which serves as a client to the data source;
2. a Ruby on Rails application which contains the destination database


### Specifications

The Ruby gem will use [LightService](https://github.com/adomokos/light-service)
to break down your ETL process.

The [Ruby on Rails application](https://git.appexpress.io/open-source/etl-starter-app) 
will use the client Ruby gem created to kickstart the ETL process and must be
able to show the loaded data using a web interface.