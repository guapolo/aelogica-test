ETL Starter Application
=======================

Instructions to run the project:

```
bundle install
bundle exec rake db:setup

# You'll need Docker running or a local Redis instance to be able to process the jobs.
docker-compose -f docker/docker-compose.yml up -d

bundle exec rails s -d
bundle exec sidekiq

open http://localhost:3000
```

Use `docker-compose -f docker/docker-compose.yml down` to stop the Redis container, and `kill $(lsof -i :3000 | awk 'NR > 1 {print $2}' | uniq)` to kill the Rails server when you're finished.


### Explanation to the choices made here

* The gem organizers take a file and an Active Record class as a parameter. Instances of such class will be used for record creation/validation.
* There's an organizer for each type/structure of a file. For the JSON files, since structure is similar differing in one, it was proper to extract logic to a module, and just use the appropriate key of the array with the units info. (see [here](lib/etl/lib/etl/extract_from_json.rb))
* The Rails app has a button to trigger the ETL process, one job for each file. This way we don't do the heavy work in the request cycle, and the jobs can be processed in many queues/worker processes as needed.